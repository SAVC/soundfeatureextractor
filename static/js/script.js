$(document).ready(function () {
    $("#doUpload").click(function () {
        var fd = new FormData();
        var files = $('#uploadFile')[0].files[0];
        fd.append('file', files);

        $.ajax({
            url: '/file/features',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response != 0) {
                    var element = $("#featuresResult");
                    element.html(response);
                    element.removeClass("hidden-element ");
                    fitToContent('featuresResult', 500)
                } else {
                    alert('Error');
                }
            },
            error: function (e) {
                alert(e.responseText)
            }
        });
    });

    function fitToContent(id, maxHeight) {
        var text = id && id.style ? id : document.getElementById(id);
        if (!text)
            return;

        var adjustedHeight = text.clientHeight;
        if (!maxHeight || maxHeight > adjustedHeight) {
            adjustedHeight = Math.max(text.scrollHeight, adjustedHeight);
            if (maxHeight)
                adjustedHeight = Math.min(maxHeight, adjustedHeight);
            if (adjustedHeight > text.clientHeight)
                text.style.height = adjustedHeight + "px";
        }
    }
});