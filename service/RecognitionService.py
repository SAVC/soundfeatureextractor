import logging

import os
import warnings

import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler

import service as service

warnings.filterwarnings('ignore')


class RecognitionService:

    def __init__(self):
        self.__soundFeatureService = service.SoundFeatureService
        self.model = None

        try:
            df = self.__load_from_csv()
            self.__train_model(df)
        except:
            logging.info("Model not found. Try to train new model from train dataset")
            self.train_model_from_dataset()
            logging.info("New model created from tran dataset. Start working...")

    def train_model_from_dataset(self):
        features = self.__extract_features()
        self.__save_to_csv(features)
        self.__train_model(self.__load_from_csv())

    def get_prediction(self, vector):
        predict_df = self.__create_df()
        self.__fill_df_from_feature(predict_df, vector, 0)
        x = predict_df.drop(columns=['Audio', 'Target'], axis=1)
        x = self.scaler.transform(x)

        return self.model.predict(x)

    def __train_model(self, df):
        y = df['Target']
        x = df.drop(columns=['Unnamed: 0', 'Audio', 'Target'], axis=1)
        self.scaler = StandardScaler().fit(x)
        x_scaled = self.scaler.transform(x)

        self.model = LogisticRegression().fit(x_scaled, y)

    def __extract_features(self):
        train_set = self.__create_df()

        train_path = 'train/'
        files = []
        for r, d, f in os.walk(train_path):
            for file in f:
                if '.wav' in file:
                    files.append(os.path.join(r, file))

        index = 0
        for f in files:
            track_number = f[6::].split('_')[0]
            is_correct = (f.lower().__contains__('good')) if 1 else 0
            train_set.at[index, 'Target'] = is_correct
            train_set.at[index, 'Audio'] = track_number
            features = self.__soundFeatureService.get_features(f)
            self.__fill_df_from_feature(train_set, features, index)
            index += 1

        return train_set

    @staticmethod
    def __fill_df_from_feature(train_set, features, index):
        for key in train_set.keys().tolist():
            if (str(key) != 'Target') & (str(key) != 'Audio'):
                value = getattr(features, key)
                train_set.at[index, key] = float(value)

    @staticmethod
    def __create_df():
        return pd.DataFrame(columns=["Target", "Audio",
                                     "autocorrelate_1_derivative_max",
                                     "autocorrelate_1_derivative_mean",
                                     "autocorrelate_1_derivative_min",
                                     "autocorrelate_2_derivative_max",
                                     "autocorrelate_2_derivative_mean",
                                     "autocorrelate_2_derivative_min",
                                     "autocorrelate_max",
                                     "autocorrelate_mean",
                                     "autocorrelate_min",
                                     "autocorrelate_normalized_1_derivative_max",
                                     "autocorrelate_normalized_1_derivative_mean",
                                     "autocorrelate_normalized_1_derivative_min",
                                     "autocorrelate_normalized_2_derivative_max",
                                     "autocorrelate_normalized_2_derivative_mean",
                                     "autocorrelate_normalized_2_derivative_min",
                                     "autocorrelate_normalized_max",
                                     "autocorrelate_normalized_mean",
                                     "autocorrelate_normalized_min",
                                     "chroma_cens_1_derivative_max",
                                     "chroma_cens_1_derivative_mean",
                                     "chroma_cens_1_derivative_min",
                                     "chroma_cens_2_derivative_max",
                                     "chroma_cens_2_derivative_mean",
                                     "chroma_cens_2_derivative_min",
                                     "chroma_cens_max",
                                     "chroma_cens_mean",
                                     "chroma_cens_min",
                                     "chroma_cqt_1_derivative_max",
                                     "chroma_cqt_1_derivative_mean",
                                     "chroma_cqt_1_derivative_min",
                                     "chroma_cqt_2_derivative_max",
                                     "chroma_cqt_2_derivative_mean",
                                     "chroma_cqt_2_derivative_min",
                                     "chroma_cqt_max",
                                     "chroma_cqt_mean",
                                     "chroma_cqt_min",
                                     "chroma_stft_1_derivative_max",
                                     "chroma_stft_1_derivative_mean",
                                     "chroma_stft_1_derivative_min",
                                     "chroma_stft_2_derivative_max",
                                     "chroma_stft_2_derivative_mean",
                                     "chroma_stft_2_derivative_min",
                                     "chroma_stft_max",
                                     "chroma_stft_mean",
                                     "chroma_stft_min",
                                     "melspectrogram_1_derivative_max",
                                     "melspectrogram_1_derivative_mean",
                                     "melspectrogram_1_derivative_min",
                                     "melspectrogram_2_derivative_max",
                                     "melspectrogram_2_derivative_mean",
                                     "melspectrogram_2_derivative_min",
                                     "melspectrogram_max",
                                     "melspectrogram_mean",
                                     "melspectrogram_min",
                                     "mfcc_10_max",
                                     "mfcc_10_min",
                                     "mfcc_11_max",
                                     "mfcc_11_min",
                                     "mfcc_12_max",
                                     "mfcc_12_min",
                                     "mfcc_13_max",
                                     "mfcc_13_min",
                                     "mfcc_14_max",
                                     "mfcc_14_min",
                                     "mfcc_15_max",
                                     "mfcc_15_min",
                                     "mfcc_16_max",
                                     "mfcc_16_min",
                                     "mfcc_17_max",
                                     "mfcc_17_min",
                                     "mfcc_18_max",
                                     "mfcc_18_min",
                                     "mfcc_1_max",
                                     "mfcc_1_min",
                                     "mfcc_2_max",
                                     "mfcc_2_min",
                                     "mfcc_3_max",
                                     "mfcc_3_min",
                                     "mfcc_4_max",
                                     "mfcc_4_min",
                                     "mfcc_5_max",
                                     "mfcc_5_min",
                                     "mfcc_6_max",
                                     "mfcc_6_min",
                                     "mfcc_7_max",
                                     "mfcc_7_min",
                                     "mfcc_8_max",
                                     "mfcc_8_min",
                                     "mfcc_9_max",
                                     "mfcc_9_min",
                                     "onset_strength_1_derivative_max",
                                     "onset_strength_1_derivative_mean",
                                     "onset_strength_1_derivative_min",
                                     "onset_strength_2_derivative_max",
                                     "onset_strength_2_derivative_mean",
                                     "onset_strength_2_derivative_min",
                                     "onset_strength_max",
                                     "onset_strength_mean",
                                     "onset_strength_min",
                                     "poly_features_0_max",
                                     "poly_features_0_min",
                                     "poly_features_1_max",
                                     "poly_features_1_min",
                                     "poly_features_2_max",
                                     "poly_features_2_min",
                                     "poly_features_3_max",
                                     "poly_features_3_min",
                                     "poly_features_4_max",
                                     "poly_features_4_min",
                                     "rms_1_derivative_max",
                                     "rms_1_derivative_mean",
                                     "rms_1_derivative_min",
                                     "rms_2_derivative_max",
                                     "rms_2_derivative_mean",
                                     "rms_2_derivative_min",
                                     "rms_max",
                                     "rms_mean",
                                     "rms_min",
                                     "spectral_bandwidth_1_derivative_max",
                                     "spectral_bandwidth_1_derivative_mean",
                                     "spectral_bandwidth_1_derivative_min",
                                     "spectral_bandwidth_2_derivative_max",
                                     "spectral_bandwidth_2_derivative_mean",
                                     "spectral_bandwidth_2_derivative_min",
                                     "spectral_bandwidth_max",
                                     "spectral_bandwidth_mean",
                                     "spectral_bandwidth_min",
                                     "spectral_centroid_1_derivative_max",
                                     "spectral_centroid_1_derivative_mean",
                                     "spectral_centroid_1_derivative_min",
                                     "spectral_centroid_2_derivative_max",
                                     "spectral_centroid_2_derivative_mean",
                                     "spectral_centroid_2_derivative_min",
                                     "spectral_centroid_max",
                                     "spectral_centroid_mean",
                                     "spectral_centroid_min",
                                     "spectral_contrast_1_derivative_max",
                                     "spectral_contrast_1_derivative_mean",
                                     "spectral_contrast_1_derivative_min",
                                     "spectral_contrast_2_derivative_max",
                                     "spectral_contrast_2_derivative_mean",
                                     "spectral_contrast_2_derivative_min",
                                     "spectral_contrast_max",
                                     "spectral_contrast_mean",
                                     "spectral_contrast_min",
                                     "spectral_flatness_1_derivative_max",
                                     "spectral_flatness_1_derivative_mean",
                                     "spectral_flatness_1_derivative_min",
                                     "spectral_flatness_2_derivative_max",
                                     "spectral_flatness_2_derivative_mean",
                                     "spectral_flatness_2_derivative_min",
                                     "spectral_flatness_max",
                                     "spectral_flatness_mean",
                                     "spectral_flatness_min",
                                     "spectral_rolloff_1_derivative_max",
                                     "spectral_rolloff_1_derivative_mean",
                                     "spectral_rolloff_1_derivative_min",
                                     "spectral_rolloff_2_derivative_max",
                                     "spectral_rolloff_2_derivative_mean",
                                     "spectral_rolloff_2_derivative_min",
                                     "spectral_rolloff_max",
                                     "spectral_rolloff_mean",
                                     "spectral_rolloff_min",
                                     "tempo",
                                     "tempogram_1_derivative_max",
                                     "tempogram_1_derivative_mean",
                                     "tempogram_1_derivative_min",
                                     "tempogram_2_derivative_max",
                                     "tempogram_2_derivative_mean",
                                     "tempogram_2_derivative_min",
                                     "tempogram_max",
                                     "tempogram_mean",
                                     "tempogram_min",
                                     "tonnetz_1_derivative_max",
                                     "tonnetz_1_derivative_mean",
                                     "tonnetz_1_derivative_min",
                                     "tonnetz_2_derivative_max",
                                     "tonnetz_2_derivative_mean",
                                     "tonnetz_2_derivative_min",
                                     "tonnetz_max",
                                     "tonnetz_mean",
                                     "tonnetz_min",
                                     "zero_crossing_rate_1_derivative_max",
                                     "zero_crossing_rate_1_derivative_mean",
                                     "zero_crossing_rate_1_derivative_min",
                                     "zero_crossing_rate_2_derivative_max",
                                     "zero_crossing_rate_2_derivative_mean",
                                     "zero_crossing_rate_2_derivative_min",
                                     "zero_crossing_rate_max",
                                     "zero_crossing_rate_mean",
                                     "zero_crossing_rate_min"
                                     ])

    @staticmethod
    def __save_to_csv(df):
        df.to_csv("train/train_set.csv", sep=';')

    @staticmethod
    def __load_from_csv():
        return pd.read_csv("train/train_set.csv", sep=';')


component = RecognitionService()
