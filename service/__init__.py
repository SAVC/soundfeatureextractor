from .FileStorageService import component as FileStorageService
from .SoundFeatureService import component as SoundFeatureService
from .RecognitionService import component as RecognitionService
from .TestService import component as TestService
