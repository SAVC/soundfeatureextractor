import os
import service as service
import model as model


class TestService:
    def __init__(self):
        self.recognitionService = service.RecognitionService
        self.soundFeatureService = service.SoundFeatureService

    def statistic(self):
        result = model.Object()

        train_path = 'test/'
        files = []
        for r, d, f in os.walk(train_path):
            for file in f:
                if '.wav' in file:
                    files.append(os.path.join(r, file))

        index = 0
        for f in files:
            features = self.soundFeatureService.get_features(f)
            predict_result = self.recognitionService.get_prediction(features)
            # result.at[index, f] = str(predict_result)
            result.__dict__[str(f)] = str(predict_result)
            index += 1

        return result


component = TestService()
