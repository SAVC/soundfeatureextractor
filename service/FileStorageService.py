import os
import shutil
import uuid

from pydub import AudioSegment


class FileStorageService:
    def __init__(self):
        self.upload_folder = 'uploads'
        self.allowed_extensions = {'wav', 'ogg', 'mp3'}
        if os.path.isdir(self.upload_folder):
            shutil.rmtree(self.upload_folder)
        os.mkdir(self.upload_folder)

    def validate_file(self, filename):
        return filename[-3:].lower() in self.allowed_extensions

    def save_file(self, file, extension):
        _id = str(uuid.uuid4())
        _filename = str(_id) + '.' + str(extension)
        file.save(os.path.join(self.upload_folder, _filename))
        return _id

    def file_path_by_id(self, _id, ext):
        return os.path.join(self.upload_folder, _id + '.' + ext)

    def remove_file_from_storage(self, _id, ext):
        _file_path = self.file_path_by_id(_id, ext)
        os.remove(_file_path)

    def file_converter(self, file):
        if file.content_type == 'audio/wav':
            return self.save_file(file, 'wav')
        elif file.content_type == 'audio/mp3':
            return self._convert_file(file, 'mp3', AudioSegment.from_mp3)
        elif file.content_type == 'audio/ogg':
            return self._convert_file(file, 'ogg', AudioSegment.from_ogg)

    def _convert_file(self, file, ext_original, func):
        _id = self.save_file(file, ext_original)
        _sound = func(self.file_path_by_id(_id, ext_original))
        _sound.export(self.file_path_by_id(_id, 'wav'), format='wav')
        self.remove_file_from_storage(_id, ext_original)
        return _id


component = FileStorageService()
