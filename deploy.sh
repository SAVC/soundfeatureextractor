rm -R /app/SoundRecognizer/sound_feature_extractor
mkdir /app/SoundRecognizer/sound_feature_extractor
cp -a . /app/SoundRecognizer/sound_feature_extractor
cd /app/SoundRecognizer || exit
. sound_recognizer_venv/bin/activate
cd sound_feature_extractor || exit
pip install -r requirements.txt