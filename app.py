import logging

from flask import Flask, request, render_template, send_from_directory

import service as service

app = Flask(__name__)
soundFeatureService = service.SoundFeatureService
recognitionService = service.RecognitionService
fileStorageService = service.FileStorageService
testService = service.TestService

HOST = '119.81.242.108'
PORT = 5000

logging.basicConfig(filename='app.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


# Static content routes
@app.route('/')
def render_static():
    return render_template('index.html')


@app.route('/<path:path>')
def send_css(path):
    s = path.split('/')
    folder = ''
    i = 0
    for index in range(len(s)):
        folder += s[index] + '/'
        i = index
    filename = s[i]

    return send_from_directory(folder, filename)


# Train routes
@app.route('/train/dataset', methods=['POST'])
def train_model_from__dataset():
    recognitionService.train_model_from_dataset()
    return 'SUCCESS'


# Get train statistic
@app.route('/train/statistic', methods=['GET'])
def statistic():
    return testService.statistic().toJSON()


# Prediction route
@app.route('/file/predict', methods=['POST'])
def predict():
    file = request.files['file']
    if file and fileStorageService.validate_file(file.filename):
        _id = fileStorageService.file_converter(file)
        _file_path = fileStorageService.file_path_by_id(_id, 'wav')
        _features = soundFeatureService.get_features(_file_path)
        result = recognitionService.get_prediction(_features)
        fileStorageService.remove_file_from_storage(_id, 'wav')
        logging.info("File " + file.filename + " processed with result = " + str(result))
        return 'SUCCESS: id = ' + _id + '\n' + 'Result = ' + str(result)


# Get features routes
@app.route('/file/features', methods=['POST'])
def features_json():
    file = request.files['file']
    return features(file).toJSON()


@app.route('/file/features/csv', methods=['POST'])
def features_csv():
    _file = request.files['file']
    return features(_file).to_csv()


def features(file):
    if file and fileStorageService.validate_file(file.filename):
        _id = fileStorageService.file_converter(file)
        _file_path = fileStorageService.file_path_by_id(_id, 'wav')
        _features = soundFeatureService.get_features(_file_path)
        fileStorageService.remove_file_from_storage(_id, 'wav')
        return _features


# System shutdown route
@app.route('/shutdown', methods=['POST'])
def shutdown():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    return 'Server shutting down...'


if __name__ == '__main__':
    app.run(host=HOST, port=PORT)
