cd /app/SoundRecognizer || exit
. sound_recognizer_venv/bin/activate
cd sound_feature_extractor || exit
nohup python app.py > /app/SoundRecognizer/log.out 2>&1 &